subset=$(cat subset)

date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=cuttlefisha-$arch-$date-subset$subset



# if we do all accessions, then comment this line..
#bash find_non_unitigged.sh

cd batch
# ..and this line
for accession in `cat ../find_non_unitigged.txt`
# alternative: do all the subset regardless of whether it was done already or not
do
    # get its size
    size=$(grep $accession ../subsets/*.tsv | awk '{print $2}')
    echo $accession $size
    python simple_submit_job.py $accession $size $tag
done
