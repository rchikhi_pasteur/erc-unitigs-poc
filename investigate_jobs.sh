myJOBQueue=IndexThePlanetJobQueue

jobstatus=succeeded

aws batch list-jobs --job-queue $myJOBQueue --job-status $jobstatus --output text --query jobSummaryList[*].[jobId] > investigate_jobs.jobs.txt

jobstatus=failed

aws batch list-jobs --job-queue $myJOBQueue --job-status $jobstatus --output text --query jobSummaryList[*].[jobId] >> investigate_jobs.jobs.txt

rm -f investigate_jobs.accessions.txt

# 50 lines at a time
while mapfile -t -n 50 ary && ((${#ary[@]})); do
	input_jobs=$(printf '%s\n' "${ary[@]}")
	input_jobs=$(echo $input_jobs | sed 's/\n//g' )
	echo "$input_jobs"
	aws batch describe-jobs --jobs $input_jobs | jq -r '.jobs | .[] | .jobId + " " + .status + " " + (.container.environment[] | select(.name == "Accession") | .value)' >> investigate_jobs.accessions.txt
done <  investigate_jobs.jobs.txt

rm -f investigate_jobs.jobs.txt
