#!/bin/bash

read -p "Will erase all unitigs in s3://serratus-rayan/indextheplanet-unitigs-poc/ before continuing. (yes/no) " response

case "$response" in
    [yY][eE][sS]|[yY])
        # Continue with the rest of the script
        ;;
    *)
        echo "Exiting the script."
        exit 1
        ;;
esac

aws s3 rm s3://serratus-rayan/indextheplanet-unitigs-poc/ --recursive
aws s3 rm s3://serratus-rayan/indextheplanet-contigs-poc/ --recursive
for i in `seq 0 9`
do
	echo $i
	echo $i > subset
	#bash process_subset.sh
	bash process_subset_array.sh
done
