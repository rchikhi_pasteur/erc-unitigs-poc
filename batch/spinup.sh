#!/bin/bash

# Check for --update argument
if [ "$1" == "--update" ]; then
    echo "updating stack.."
    aws cloudformation update-stack --stack-name serratus-batch-unitigs --template-body file://template.yaml --capabilities CAPABILITY_NAMED_IAM
else
    aws cloudformation create-stack --stack-name serratus-batch-unitigs --template-body file://template.yaml --capabilities CAPABILITY_NAMED_IAM
fi

