# Construction of unitigs on AWS Batch

### Source

Taken from Serratus assembly AWS batch workflow

### Installation 

You need to manually import the VPC stack (`0.VPC-Large-Scale.yaml`) into CloudFormation. Name that stack "Logan-LargeScaleVPC". Once the VPC is created:
 
Execute the below commands to spin up the infrastructure cloudformation stack.

```
./spinup.sh
./deploy-docker.sh
```

If you ever recreate the stack (e.g. after `cleanup.sh`), you don't need to run `deploy-docker.sh` unless the Dockerfile or scripts in `src/` were modified.

### Running a batch

1. `python simple_submit_job.py [accession] [accession_size] [tag]`
2. In AWS Console > Batch, monitor how the job runs.

### Code Cleanup

In short:

```
./cleanup.sh
```

Which deletes the CloudFormation stack.

What it doesn't do (needs to be done manually):

AWS Console > ECR - serratus-batch-unitigs-job - delete the image(s) that are pushed to the repository

## License

This library is licensed under the MIT-0 License. See the LICENSE file.

