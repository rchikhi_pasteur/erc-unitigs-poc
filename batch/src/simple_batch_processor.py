# constructs unitigs and and uploads them
# derivative of serratus-batch-assembly

import boto3
from boto3.dynamodb.conditions import Key, Attr
import csv, sys, argparse
from datetime import datetime
import json
import os
import urllib3
import glob
import constants
import subprocess

import reads_sra
import unitigs 
import seqstats
import contigs
from utils import sdb_log, get_machine_architecture

LOGTYPE_ERROR = 'ERROR'
LOGTYPE_INFO = 'INFO'
LOGTYPE_DEBUG = 'DEBUG'


def process_file(accession, region, nb_threads):

    urllib3.disable_warnings()
    arch = get_machine_architecture()
    s3 = boto3.client('s3')
    sdb = boto3.client('sdb', region_name=region)
    outputBucket = "serratus-rayan"
    s3_folder         = "indextheplanet-unitigs-poc/" + accession + "/"
    s3_folder_contigs = "indextheplanet-contigs-poc/" + accession + "/"
    retval = 0

    print("docker image - poc", flush=True)
    print("region - " + region, flush=True)
    print("accession - " + accession, flush=True)

    try:
        print("instance type - " + subprocess.check_output(
            ['curl', '-s', 'http://169.254.169.254/latest/meta-data/instance-type'])
              .decode('utf-8').strip())
    except Exception as e:
        return f"Failed to retrieve instance type: {e}"
 
    try:
        print("instance ID - " + subprocess.check_output(
            ['curl', '-s', 'http://169.254.169.254/latest/meta-data/instance-id'])
              .decode('utf-8').strip())
    except Exception as e:
        return f"Failed to retrieve instance ID: {e}"
    
    # check free space
    os.chdir("/")
    print("free space on /", flush=True)
    os.system(' '.join(["df", "-h", "."]))
    
    if os.path.exists("/serratus-data/"+accession):
        print(f"huh? path /serratus-data/{accession} already exists?!")
        print("How's that possible, unless you ran the same job at the same time, same machine?")
        retval = constants.UNKNOWN_ERROR
        return retval

    # necessary because cuttlefish uses the same temp files names!
    os.mkdir("/serratus-data/"+accession)
    os.chdir("/serratus-data/"+accession)
    print("free space on /serratus-data/"+accession, flush=True)
    os.system(' '.join(["df", "-h", "."]))

    startBatchTime = datetime.now()
    
    reads_retval, max_disk = reads_sra.get_reads(accession, s3, s3_folder, outputBucket, sdb, nb_threads)
    retval |= reads_retval

    if retval > 0:
        if sdb is not None: sdb_log(sdb,accession,arch+'_return_value',retval)
        return retval
    
    #local_file = accession + ".fasta"
    local_file = accession + ".sra"

    # check if fasta was written (maybe it wasn't, because there of no reads)
    if os.path.exists(local_file):
        unitigs_retval, unitigs_max_disk = unitigs.unitigs(accession, local_file, outputBucket, s3_folder, sdb, nb_threads)
        retval |= unitigs_retval
        max_disk = max(max_disk, unitigs_max_disk)
    
    if retval > 0:
        if sdb is not None: sdb_log(sdb,accession,arch+'_return_value',retval)
        return retval

    # compute n50 etc
    seqstats_retval = seqstats.seqstats(accession, sdb, what="unitigs")
    retval |= seqstats_retval

    # make contigs on the way
    contigs_retval, contigs_max_disk = contigs.contigs(accession, outputBucket, s3_folder_contigs, sdb, nb_threads)
    retval |= contigs_retval
    max_disk = max(max_disk, contigs_max_disk)
    
    # compute n50 of contigs
    seqstats_retval = seqstats.seqstats(accession, sdb, what="contigs")
    retval |= seqstats_retval

    # finishing up
    endBatchTime = datetime.now()
    diffTime = endBatchTime - startBatchTime
    print(accession, "Complete pipeline processing time - " + str(diffTime.seconds), flush=True) 
    if sdb is not None: sdb_log(sdb,accession,arch+'_return_value',retval)
    if retval == 0:
        if sdb is not None: sdb_log(sdb,accession,arch+'_processing_time',diffTime.seconds)
        if sdb is not None: sdb_log(sdb,accession,arch+'_max_disk',max_disk)

    return retval

def process_accession(accession, region, nb_threads):
    retval = 0
    try:
        retval |= process_file(accession, region, nb_threads)
    except Exception as ex:
        print("Exception occurred during process_file() with arguments", accession, region,flush=True) 
        print(ex,flush=True)
        import traceback
        traceback.print_exc()
        sys.stdout.flush()
        retval = constants.UNKNOWN_ERROR

    # it is important that this code isn't in process_file() as that function may stop for any reason
    print("retval",retval,flush=True)
    print("checking free memory",flush=True)
    os.system(' '.join(["free","-m"])) 
    print("checking free space on /serratus-data",flush=True)
    os.chdir("/serratus-data")
    os.system(' '.join(["ls","-Ral",accession+"*"])) 
    os.system(' '.join(["df", "-h", "."]))
    os.system(' '.join(["df", "-h", "/"]))

    #cleanup
    os.system(' '.join(["rm","-Rf","/serratus-data/"+accession]))
    return retval


def main():
    accessions = ""
    region = "us-east-1"
    nb_threads = str(4)
   
    if "Accessions" in os.environ:
        accessions = os.environ.get("Accessions")
    if "Region" in os.environ:
        region = os.environ.get("Region")
    if "Threads" in os.environ:
        nb_threads = os.environ.get("Threads")

    if len(accessions) == 0:
        exit("This script needs an environment variable Accessions set to something. It may be just one accession")

    print("accessions:", accessions, "region: " + region, "threads:" + nb_threads, flush=True)

    retval = 0
    for accession in accessions.split(','):
        retval |= process_accession(accession, region, nb_threads)
    if retval > 0:
        sys.exit(retval)

if __name__ == '__main__':
   main()
