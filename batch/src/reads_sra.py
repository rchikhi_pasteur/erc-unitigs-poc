import os
import boto3
from utils import sdb_log
from datetime import datetime
import constants

def get_reads(accession, s3, s3_folder, outputBucket, sdb, nb_threads):

    print("downloading reads from S3 directly in SRA format..",flush=True)
    startTime = datetime.now()
    
    ret = 0
    file_size = 0
    
    s3_path= f"s3://sra-pub-run-odp/sra/{accession}/{accession}"

    try:
        s3 = boto3.resource('s3')
        bucket_name, file_path = s3_path.replace('s3://', '').split('/', 1)
        bucket = s3.Bucket(bucket_name)
        local_file = file_path.split('/')[-1]+".sra"
        bucket.download_file(file_path, local_file)
        file_size = str(os.stat(local_file).st_size)
    except Exception as e:
        print("SRA download error:",str(e),'accession',accession,'s3_path',s3_path)
        if "An error occurred (404)" in str(e):
            try:
                print("Accession not found using the regular S3 url. Retrying using prefetch..")
                os.system('vdb-config --prefetch-to-cwd')
                os.system('vdb-config --report-cloud-identity yes')
                os.system('vdb-config --simplified-quality-scores yes')
                local_file = accession + ".sra"
                pret = os.system('prefetch --max-size 500G ' + accession + ' -o ' + local_file)
                file_size = str(os.stat(local_file).st_size)
                if pret > 0:
                    print("SRA download failed using prefetch")
                    ret = constants.PREFETCH_FAILED 
            except:
                print("SRA download exception using prefetch:",str(e),'accession',accession,'s3_path',s3_path)
                ret = constants.PREFETCH_FAILED 
        else:
            ret = constants.PREFETCH_FAILED 

    endTime = datetime.now()
    diffTime = endTime - startTime
    if ret == 0:
        if sdb is not None: sdb_log(sdb,accession,'reads_sra_size',file_size)
        if sdb is not None: sdb_log(sdb,accession,'reads_sra_time',str(diffTime.seconds))
        if sdb is not None: sdb_log(sdb,accession,'reads_sra_date',str(datetime.now()))
    print(accession, "SRA Reads processing time - " + str(diffTime.seconds),flush=True) 

    return ret, 0
