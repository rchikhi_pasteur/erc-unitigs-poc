from datetime import datetime
from utils import sdb_log, get_memory_and_time, get_machine_architecture
import subprocess
import os
import re
import constants
import boto3



def unitigs(accession, local_file, outputBucket, s3_folder, sdb, nb_threads):
    start_time = datetime.now()
    #program="ggcat"
    program="cuttlefish"
    #program="bcalm2"
    arch = get_machine_architecture()
    k_value = str(31)
    ret = constants.UNITIGS_FAILED
    max_disk = 0
    #os.system("ulimit -n 49152") #should be enough for anybody, as 192 threads divided by 8 is 24, 2048*24=49152
    os.system("ulimit -n 2048") 

    if program == "ggcat":
        output_filename = accession+".unitigs.fa.lz4"
        try:
            runtime, mem, percent_cpu, max_disk, stderr, stdout = get_memory_and_time(["/usr/bin/time", 
                            "/root/.cargo/bin/ggcat", 
                            "build", 
                            local_file,
                            "-k",k_value,
                            "-j",nb_threads,"-o",output_filename])
            if sdb is not None: sdb_log(sdb,accession,'ggcat_'+arch+'_mem',mem)
            if sdb is not None: sdb_log(sdb,accession,'ggcat_'+arch+'_time',runtime)
            if sdb is not None: sdb_log(sdb,accession,'ggcat_'+arch+'_percent_cpu',percent_cpu)
            ret = 0
        except subprocess.CalledProcessError:
            print(f"Ggcat failed.")
    elif program == "cuttlefish":
        output_filename = accession+".unitigs"
        max_attempts = 2
        for attempt in range(1, max_attempts + 1):
            try:
                runtime, mem, percent_cpu, max_disk, stderr, stdout = get_memory_and_time(["/cuttlefish/bin/cuttlefish", 
                                   "build",
                                   "-m",str((5*int(nb_threads))//4),
                                   "-s",local_file,"-k",k_value,"-t",nb_threads,"--read",
                                   "-o",output_filename])
                if sdb is not None: sdb_log(sdb,accession,'cuttlefisha_'+arch+'_mem',mem)
                if sdb is not None: sdb_log(sdb,accession,'cuttlefisha_'+arch+'_time',runtime)
                if sdb is not None: sdb_log(sdb,accession,'cuttlefisha_'+arch+'_percent_cpu',percent_cpu)
                print(f"Cuttlefish succeeded on attempt {attempt}.")
                print(f"Max disk: {max_disk}")
                ret = 0
                break
            except subprocess.CalledProcessError as e:
                print(f"Cuttlefish failed on attempt {attempt}.")
                # just make sure it's not one of those empty datasets
                import re
                if re.search(r'Number\s+of\s+solid\s+32-mers:\s+0\.', e.stdout):
                    print("Ah, but the dataset has 0 k-mers.")
                    os.system("touch "+output_filename+".fa") #useful for my later scripts that check if the unitigs were created or not
                    ret = 0
                    break
                print(f"Command: {e.cmd}")
                print(f"Return Code: {e.returncode}")
                print("----")
                print(f"Stdout: {e.stdout}")
                print("----")
                print(f"Stderr: {e.stderr}")
                print("----")
                if attempt < max_attempts: print("Retrying...")
        #os.system(' '.join(["lz4","-z",output_filename+".fa", output_filename+".fa.lz4"]))
        #output_filename += ".fa.lz4"
        output_filename += ".fa"
    elif program == "bcalm2":
        output_filename = accession+".unitigs"
        try:
            runtime, mem, percent_cpu, max_disk, stderr, stdout = get_memory_and_time(["/bcalm/build/bcalm", 
                                "-in",local_file,"-kmer-size",k_value,"-nb-cores",nb_threads,
                                "-out",accession])
            if sdb is not None: sdb_log(sdb,accession,'bcalm2_'+arch+'_mem',mem)
            if sdb is not None: sdb_log(sdb,accession,'bcalm2_'+arch+'_time',runtime)
            if sdb is not None: sdb_log(sdb,accession,'bcalm2_'+arch+'_percent_cpu',percent_cpu)
            print(f"Bcalm succeeded.")
            ret = 0
        except subprocess.CalledProcessError:
            print(f"Bcalm failed.")
        #os.system(' '.join(["lz4","-z",output_filename+".fa", output_filename+".fa.lz4"]))
        #output_filename += ".fa.lz4"
        output_filename += ".fa"

    else:
        exit("unknown program: " + binary_name)

    unitigs_time = datetime.now() - start_time

    if os.path.exists(output_filename):
        s3 = boto3.client('s3') # recreate the client, who knows, possibly the previous one timeout'd?
        s3.upload_file(output_filename, outputBucket, s3_folder + output_filename, ExtraArgs={'ACL': 'public-read'})

    return ret, max_disk


