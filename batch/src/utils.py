import subprocess
import time
import threading
import os
import boto3

# check if a file exists
def s3_file_exists(s3, bucket, prefix):
    res = s3.list_objects_v2(Bucket=bucket, Prefix=prefix, MaxKeys=1)
    return 'Contents' in res

# sdb logging parameters
domain_name = "serratus-batch-unitigs"

def sdb_log(
        sdb, item_name, name, value,
        region='us-east-1'
    ):
        """
        Insert a single record to simpledb domain.
        PARAMS:
        @item_name: unique string for this record.
        @attributes = [
            {'Name': 'duration', 'Value': str(duration), 'Replace': True},
            {'Name': 'date', 'Value': str(date), 'Replace': True},
        ]
        """
        try:
            status = sdb.put_attributes(
                DomainName=domain_name,
                ItemName=str(item_name),
                Attributes=[{'Name':str(name), 'Value':str(value), 'Replace': True}]
            )
        except Exception as e:
            print("SDB put_attribute error:",str(e),'domain_name',domain_name,'item_name',item_name)
            status = False
        try:
            if status['ResponseMetadata']['HTTPStatusCode'] == 200:
                return True
            else:
                print("SDB log error:",status['ResponseMetadata']['HTTPStatusCode'])
                return False
        except:
            print("SDB status structure error, status:",str(status))
            return False

def stream_output(pipe, target_list, is_stderr=False):
    for line in iter(pipe.readline, ''):
        target_list.append(line)
        if is_stderr:
            print(f"stderr: {line.strip()}", flush=True)

# ddb logging parameters
table_name = "Logan"

def ddb_log(
        item_name, name, value,
        region='us-east-1'
    ):
    """
    Insert a single record to DynamoDB table.
    PARAMS:
    @table_name: Name of the DynamoDB table
    @item_name: unique string for this record (Primary Key).
    @name, @value: attributes to be inserted.
    """
    # Initialize a session using Amazon DynamoDB
    session = boto3.Session(
        region_name=region
    )

    # Initialize DynamoDB resource
    dynamodb = session.resource('dynamodb')
   
    # Select your DynamoDB table
    table = dynamodb.Table(table_name)
    
    try:
        # Put item into table
        response = table.update_item(
            Key={
                'accession': item_name
                },
            UpdateExpression=f"SET {name} = :val",
            ExpressionAttributeValues={
                ':val': str(value)
                },
        )
        
        # Check for successful response
        if response['ResponseMetadata']['HTTPStatusCode'] == 200:
            return True
        else:
            print("DynamoDB log error:", response['ResponseMetadata']['HTTPStatusCode'])
            return False
        
    except botocore.exceptions.ClientError as e:
        print("DynamoDB put_item error:", e.response['Error']['Message'], 'table_name', table_name, 'item_name', item_name)
        return False

def stream_output(pipe, target_list, is_stderr=False):
    print_stderr = False
    for line in iter(pipe.readline, ''):
        target_list.append(line)
        if is_stderr:
            if print_stderr:
                print(f"stderr: {line.strip()}", flush=True)
        else:
            print(line.strip(), flush=True)
    pipe.close()

#variable to store the maximum disk usage
max_disk_usage = 0
disk_monitor_stop = None

def get_disk_usage(path="."):
    """Return disk usage of the specified path."""
    # no, not that way, will be confused by multiple jobs
    #usage = shutil.disk_usage(path)
    #return int(usage.used / (1024 * 1024)) # in MB
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(path):
        for file in filenames:
            file_path = os.path.join(dirpath, file)
            try:
                total_size += os.path.getsize(file_path)
            except:
                pass
    return int(total_size / (1024 * 1024)) # in MB

def monitor_disk_usage(interval=5, path="."):
    """Monitor disk usage at specified interval and store the maximum usage."""
    global max_disk_usage, disk_monitor_stop
    while not disk_monitor_stop:
        usage = get_disk_usage(path)
        max_disk_usage = max(max_disk_usage, usage)
        time.sleep(interval)

def get_memory_and_time(command):
    global disk_monitor_stop, max_disk_usage

    # Construct the full command with /usr/bin/time to get memory usage and other stats
    full_command = ["/usr/bin/time", "-v"] + command
    print("Executing command: "," ".join(full_command), flush=True)
    
    start_time = time.time()
    
    # Using Popen to get real-time output

    #os.environ["LD_PRELOAD"] = "/usr/lib/libmimalloc.so"
    #proc = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, env=os.environ)
    proc = subprocess.Popen(full_command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    stdout_data = []
    stderr_data = []

    stdout_thread = threading.Thread(target=stream_output, args=(proc.stdout, stdout_data))
    stderr_thread = threading.Thread(target=stream_output, args=(proc.stderr, stderr_data, True))

    stdout_thread.start()
    stderr_thread.start()

    disk_monitor_thread = threading.Thread(target=monitor_disk_usage)
    disk_monitor_thread.start()
    disk_monitor_stop = False

    stdout_thread.join()
    stderr_thread.join()

    disk_monitor_stop = True
    #disk_monitor_thread.join() #no need to wait
    
    end_time = time.time()

    retcode = proc.wait()
    if retcode:
        error = subprocess.CalledProcessError(proc.returncode, full_command)
        error.stdout = ''.join(stdout_data)
        error.stderr = ''.join(stderr_data)
        raise error

    elapsed_time = end_time - start_time
    
    print(f"Command succeeded in {elapsed_time:.1f} seconds with error code {retcode}.", flush=True)
    #print(f"Last 15 lines stderr:", flush=True) # not interesting because /usr/bin/time
    #print(''.join(stderr_data[-15:]), flush=True)

    memory = 0
    percent_cpu = 0
    for line in stderr_data:
        if 'Maximum resident set size' in line:
            memory = int(line.split()[-1])  # Grab the last value which is the memory in Kbytes
            memory = memory / 1024 # Convert to megabytes
        if 'Percent of CPU this job got' in line:
            percent_cpu = line.split()[-1][:-1]
    
    print(f"Max RSS: {memory:.1f} MB", flush=True)
    print(f"CPU: {percent_cpu}%", flush=True)
    print(f"Max disk: {max_disk_usage:.1f} MB", flush=True)
    
    return elapsed_time, memory, int(percent_cpu), max_disk_usage, ''.join(stderr_data), ''.join(stdout_data)

import platform

def get_machine_architecture():
    architecture = platform.machine()
    
    if architecture == "x86_64":
        return "x64"
    elif architecture in ["aarch64", "ARM64"]:
        return "ARM64"
    else:
        return "Unknown"

