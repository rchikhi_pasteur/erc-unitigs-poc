from datetime import datetime
from utils import sdb_log, get_memory_and_time, get_machine_architecture
import subprocess
import os
import re
import constants
import boto3

def contigs(accession, outputBucket, s3_folder, sdb, nb_threads):
    start_time = datetime.now()
    arch = get_machine_architecture()
    k_value = str(31)
    ret = constants.CONTIGS_FAILED
    max_disk = 0

    unitigs_file = accession+".unitigs.fa"
    output_filename = accession+".contigs.fa"
    if not os.path.exists(unitigs_file) or \
            os.path.getsize(unitigs_file) == 0:
        print(f"no need to run Minia, because no unitigs.", flush=True)
        os.system("touch "+output_filename) #useful for my later scripts that check if the contigs were created or not
        ret = 0
    else:
        try:
            # hack: minia wants an input, but it doesnt matter as long as it has one solid kmer
            os.system('echo -e ">1\nAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" > ' + accession)
            os.system("sed -i 's/ka:/km:/g' " + unitigs_file)
            runtime, mem, percent_cpu, max_disk, stderr, stdout = get_memory_and_time(["/minia/build/bin/minia", 
                               "-in",accession,"-nb-cores",nb_threads,"-max-memory",str(1),
                               "-skip-bcalm","-skip-bglue","-redo-links"])
            if sdb is not None: sdb_log(sdb,accession,'minia_'+arch+'_mem',mem)
            if sdb is not None: sdb_log(sdb,accession,'minia_'+arch+'_time',runtime)
            if sdb is not None: sdb_log(sdb,accession,'minia_'+arch+'_percent_cpu',percent_cpu)
            print(f"Minia succeeeded.")
            print(f"Max disk: {max_disk}")
            ret = 0
        except subprocess.CalledProcessError as e:
            # just make sure it's not one of those empty datasets
            import re
            if re.search(r'EXCEPTION:\serror\sopening\sfile:', e.stderr):
                print("Ah, but it's a case when no contigs are output.")
                os.system("touch "+output_filename) #useful for my later scripts that check if the contigs were created or not
                ret = 0
            else:
                print(f"Minia failed.")
                print(f"Command: {e.cmd}")
                print(f"Return Code: {e.returncode}")
                print("----")
                print(f"Stdout: {e.stdout}")
                print("----")
                print(f"Stderr: {e.stderr}")
                print("----")

    contigs_time = datetime.now() - start_time

    if os.path.exists(output_filename):
        s3 = boto3.client('s3') # recreate the client, who knows, possibly the previous one timeout'd?
        s3.upload_file(output_filename, outputBucket, s3_folder + output_filename, ExtraArgs={'ACL': 'public-read'})

    return ret, max_disk


