#!/bin/bash
set -e

aws sts get-session-token --duration-seconds 900 > credentials.json

echo "AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' credentials.json)" > credentials.env
echo "AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' credentials.json)" >> credentials.env 
echo "AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' credentials.json)" >> credentials.env 

NAME=batch-unitigs-test-local-job
#NOCACHE=--no-cache
docker build $NOCACHE -t $NAME \
             --build-arg AWS_DEFAULT_REGION=us-east-1 \
              .

# SRR11366719 is a small file
# SRR9156994 is 1 Gbases
# SRR7696661 is 27 Gbases yeast
# SRR21012863 has no solid 32-mers
# SRR709914 has no reads (fasterq-dump fails)
# SRR10912254 has unitigs but no contigs
# DRR075766 is one that repeatedly OOMs
# ERR589596 another one that OOMs
memorylimit="--memory=8g"
docker run \
    --ulimit nofile=65535:65535 \
    --env-file credentials.env \
    -e AWS_DEFAULT_REGION=us-east-1\
    -e Accessions=SRR19911298\
    $memorylimit \
    $NAME \
    python simple_batch_processor.py 

rm -f credentials.json credentials.env
