import boto3
import sys

def submit_array_job(num_jobs, tag, s3_path, job_definition, threads):
    batch = boto3.client('batch')
    region = "us-east-1"
    
    response = batch.submit_job(
        jobName=tag,
        jobQueue='IndexThePlanetJobQueue',
        arrayProperties={'size': num_jobs},
        jobDefinition=job_definition,
        tags={"IndexThePlanet": tag},
        propagateTags=True,
        timeout={
            'attemptDurationSeconds': int(threads)*2500 
        },
        containerOverrides={
            "command": ["python", "array_batch_processor.py"],
            "environment": [
                {"name": "S3_PATH", "value": s3_path},
                {"name": "Region", "value": region},
                {"name": "Threads", "value": threads},
            ]
        }
    )

    print(f"Job ID is {response['jobId']}.")

if __name__ == '__main__':
    if len(sys.argv) < 6:
        exit("Usage: python array_submit_job.py <num_jobs> <tag> <s3_path> <job_definition> <num_threads>")
    
    num_jobs = int(sys.argv[1])
    tag = sys.argv[2]
    s3_path = sys.argv[3]
    job_definition = sys.argv[4]
    threads = sys.argv[5]

    submit_array_job(num_jobs, tag, s3_path, job_definition, threads)
