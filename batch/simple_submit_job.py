import json
import boto3
import sys

if len(sys.argv) < 4:
    exit("argument: [accessions] [accession_size] [tag]")

accessions = sys.argv[1]
accession_size = sys.argv[2]
tag = sys.argv[3]
if "RR" not in accessions:
    exit("accessions should be of the form: (E/S)RRxxxx,(E/S)RRxxxx,... A single accession is fine too.")
if not accession_size.isnumeric():
    exit("accession size should be a number")
if tag.isnumeric():
    exit("tag shouldn't be just a number")

batch = boto3.client('batch')
#region = batch.meta.region_name
region = "us-east-1"

if int(accession_size) < 20000:
    jobDefinition = 'indextheplanet-8gb-job'
    threads = str(4) 
elif int(accession_size) < 100000:
    jobDefinition = 'indextheplanet-15gb-job'
    threads = str(8) 
else:
    jobDefinition = 'indextheplanet-31gb-job' 
    threads = str(16)


response = batch.submit_job(jobName=tag+'-'+accessions, 
                            jobQueue='IndexThePlanetJobQueue', 
                            jobDefinition=jobDefinition, 
                            tags={"IndexThePlanet": tag},
                            propagateTags=True, #unfortunately it doesn't propagate to the EC2 instance :(
                            containerOverrides={
                                "command": [ "python", "simple_batch_processor.py"],
                                "environment": [ 
                                    {"name": "Accessions", "value": accessions},
                                    {"name": "Region", "value": region},
                                    {"name": "Threads", "value": threads },
                                ]
                            })


print("Job ID is {}.".format(response['jobId']))
