import boto3
import csv
import time
from botocore.exceptions import ClientError
import sys
import concurrent.futures

# Initialize a session using Amazon DynamoDB.
session = boto3.Session(
    region_name='us-east-1'
)

# Initialize DynamoDB resource.
dynamodb = session.resource('dynamodb')

# DynamoDB table name.
table_name = 'Logan'
table = dynamodb.Table(table_name)

def batch_write(items):
    try:
        with table.batch_writer() as batch:
            for item in items:
                batch.put_item(Item=item)
    except ClientError as e:
        print(f"Error in batch writing: {e}")

def exponential_backoff(items, max_retries=5):
    for attempt in range(max_retries):
        try:
            batch_write(items)
            return
        except ClientError as e:
            print(f"Error: {e}, Retrying {attempt+1}/{max_retries}...")
            time.sleep((2 ** attempt) + (random.randint(0, 1000) / 1000))

def worker(items):
    exponential_backoff(items)

with open(sys.argv[1], mode='r') as file:
    csv_reader = csv.reader(file)
    items = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=200) as executor:
        futures = []
        for row in csv_reader:
            try:
                Run,ReleaseDate,LoadDate,spots,bases,spots_with_mates,avgLength,size_MB,AssemblyName,download_path,Experiment,LibraryName,LibraryStrategy,LibrarySelection,LibrarySource,LibraryLayout,InsertSize,InsertDev,Platform,Model,SRAStudy,BioProject,Study_Pubmed_id,ProjectID,Sample,BioSample,SampleType,TaxID,ScientificName,SampleName,g1k_pop_code,source,g1k_analysis_group,Subject_ID,Sex,Disease,Tumor,Affection_Status,Analyte_Type,Histological_Type,Body_Site,CenterName,Submission,dbgap_study_accession,Consent,RunHash,ReadHash = row
            except:
                print("Error unpacking values for row, skipping:",row)
            item = {
                'accession': Run,
                'ReleaseDate': ReleaseDate,
                'size_MB': size_MB,
                'download_path': download_path
            }
            items.append(item)
            if len(items) == 25:  # Batch size to write.
                futures.append(executor.submit(worker, items.copy()))
                items.clear()

        # Writing remaining items
        if items:
            futures.append(executor.submit(worker, items.copy()))

        # Wait for all threads to complete
        for future in concurrent.futures.as_completed(futures):
            future.result()
