#!/bin/bash

subset=$(cat subset)

date=$(date +"%b%d-%Y")
arch=$(uname -m)
tag=cuttlefisha_sra-$arch-$date-subset$subset

rm -f array_8gb.txt array_15gb.txt array_31gb.txt

# Execute helper grouping script and save outputs to separate files based on accession_size
python group_accessions.py subsets/subset_$subset.tsv | while IFS=$'\t' read -r accessions size; do
	if (( size < 20000 )); then
		echo "$accessions" >> array_8gb.txt
	elif (( size >= 20000 )) && (( size < 60000 )); then
		echo "$accessions" >> array_15gb.txt
	else
		echo "$accessions" >> array_31gb.txt
	fi
done

# Upload files to S3 with a unique identifier (e.g., timestamp)
timestamp=$(date +"%Y%m%d%H%M%S")_subset$(cat subset)_$$_$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)
aws s3 cp array_8gb.txt s3://serratus-rayan/indextheplanet-unitigs-jobarray/array_${timestamp}_8gb.txt
if [ -f array_15gb.txt ]; then
aws s3 cp array_15gb.txt s3://serratus-rayan/indextheplanet-unitigs-jobarray/array_${timestamp}_15gb.txt
fi
if [ -f array_31gb.txt ]; then
aws s3 cp array_31gb.txt s3://serratus-rayan/indextheplanet-unitigs-jobarray/array_${timestamp}_31gb.txt
fi


# Get the number of lines in each file
lines_8gb=$(wc -l < array_8gb.txt)
if [ -f array_15gb.txt ]; then
lines_15gb=$(wc -l < array_15gb.txt)
fi
if [ -f array_31gb.txt ]; then
lines_31gb=$(wc -l < array_31gb.txt)
fi


# Submit job arrays
python batch/array_submit_job.py "$lines_8gb" $tag "s3://serratus-rayan/indextheplanet-unitigs-jobarray/array_${timestamp}_8gb.txt" "indextheplanet-8gb-job" 4
if [ -f array_15gb.txt ]; then
    if [ "$lines_15gb" -eq 1 ]; then
        accession=$(cat array_15gb.txt)
        echo "special case, only 1 accession in the 15gb queue: $accession"
        python batch/simple_submit_job.py $accession 90000 $tag
    else
        python batch/array_submit_job.py "$lines_15gb" $tag "s3://serratus-rayan/indextheplanet-unitigs-jobarray/array_${timestamp}_15gb.txt" "indextheplanet-15gb-job" 8
    fi
fi
if [ -f array_31gb.txt ]; then
    if [ "$lines_31gb" -eq 1 ]; then
        accession=$(cat array_31gb.txt)
        echo "special case, only 1 accession in the 31gb queue: $accession"
        python batch/simple_submit_job.py $accession 500000 $tag
    else
        python batch/array_submit_job.py "$lines_31gb" $tag "s3://serratus-rayan/indextheplanet-unitigs-jobarray/array_${timestamp}_31gb.txt" "indextheplanet-31gb-job" 16
    fi
fi

rm -f array_8gb.txt array_15gb.txt array_31gb.txt

