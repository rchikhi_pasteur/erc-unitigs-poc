import glob
import subprocess

g = open("list_restricted.txt","w")

for filename in glob.glob("subsets/*.tsv"):
    with open(filename) as f:
        for line in f:
            accession = line.split()[0]
            if accession == "Run": continue
            print(accession)
            cmd=['sra-stat','--quick',accession]
            result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

            print(f'Return code: {result.returncode}')
            print(f'Stdout:\n{result.stdout}')
            print(f'Stderr:\n{result.stderr}')
            for line in result.stderr.split('\n'):
                if "please request permission" in line:
                    assert(result.returncode != 0)
                    g.write(f"{accession}\n")
g.close()
