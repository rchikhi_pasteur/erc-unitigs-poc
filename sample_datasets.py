import pandas as pd
import numpy as np
import random
import os

# Adjust these parameters as needed
#file_path = 'SRA_Run_Members.live.tab'   # Path to your file
file_path = 'Athena_Sept_19_public.acc_mbases.txt'
delimiter = '\t'                 # Delimiter used in your file
#target_sum = 1_000_000_000_000  # 1 trillion
target_sum = 1_000_000  # 1 TB given that acc_mbases in in megabases
#column_index = 6                # Column 7 (Python uses zero-based indexing)
column_index = 1                # Column 7 (Python uses zero-based indexing)
#tolerance = 100_000_000_000     # Tolerance for the sum approximation
tolerance = 100_000     # Tolerance for the sum approximation

# too slow
def slow_sample_and_check_sum(df, target_sum, column_name, tolerance):
    sample_size = int(target_sum / df[column_name].mean())
    subset = df.sample(n=sample_size, replace=False)
    subset_sum = subset[column_name].sum()

    if abs(subset_sum - target_sum) <= tolerance:
        return subset
    return None

def iterative_random_sample(df, target_sum, column_name, tolerance):
    current_sum = 0
    selected_indices = set()
    max_iterations = len(df) * 10

    for _ in range(max_iterations):
        random_index = random.randint(0, len(df) - 1)
        if random_index not in selected_indices:
            selected_indices.add(random_index)
            current_sum += int(df.iloc[random_index][column_name])

            if abs(current_sum - target_sum) <= tolerance:
                break
            if current_sum > target_sum + tolerance:
                break

    if abs(current_sum - target_sum) <= tolerance:
        return df.iloc[list(selected_indices)]
    return None

# Save the subset to a CSV file
def save_subset_to_csv(subset, nb_saved):
    if not os.path.exists("subsets"):
        os.mkdir("subsets")

    file_name = f"subsets/subset_{nb_saved}.tsv"
    subset.to_csv(file_name, index=False, sep="\t")
    print(f"Subset saved to {file_name}")

# Read and parse the large file using chunksize to save memory
chunksize = 1_000_000
chunks = pd.read_csv(file_path, delimiter=delimiter, chunksize=chunksize)
df = pd.concat(chunk for chunk in chunks)

print(f"data loaded, {len(df)} rows")

column_name = df.columns[column_index]
print("scanning column",column_name)

df[column_name] = df[column_name].replace('-', np.nan)
df = df.dropna(subset=[column_name])
print(f"removed bad rows, {len(df)} rows")

result = None
max_iterations = 100
iterations = 0
nb_to_save = 10
nb_saved = 0

while nb_saved < nb_to_save and  iterations < max_iterations:
    result = iterative_random_sample(df, target_sum, column_name, tolerance)
    if result is not None:
        save_subset_to_csv(result, nb_saved)
        nb_saved += 1
    iterations += 1



