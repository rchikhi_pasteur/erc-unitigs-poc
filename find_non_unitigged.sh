subset=$(cat subset)

export what=unitigs
#export what=contigs

process_subset() {
    subset=$1

    awk '{print $1}' subsets/subset_$subset.tsv  |grep -v Run |grep -v "acc"|sort > subsets/subset_$subset.txt
    targetlist=subsets/subset_$subset.txt
    folder=s3://serratus-rayan/indextheplanet-$what-poc/
    aws s3 ls $folder --recursive |grep $what.fa |awk '{print $4}' |cut -d"/" -f3|cut -d. -f1 |sort |grep -Fwf subsets/subset_$subset.txt > find_non_unitigged.subset$subset.unitigged.txt
    diff $targetlist find_non_unitigged.subset$subset.unitigged.txt | grep "<" |awk '{print $2}' >> find_non_unitigged.subset$subset.txt
	rm -f find_non_unitigged.subset$subset.unitigged.txt
    echo "processed subset $subset."
}
export -f process_subset

seq 0 9 | parallel process_subset

cat find_non_unitigged.subset*.txt > find_non_unitigged.txt
rm -f find_non_unitigged.subset*.txt

echo "done, $(wc -l find_non_unitigged.txt | awk '{print $1}') $what accessions not present. See list in find_non_unitigged.txt"


# some stats:

# Step 1: Read the missing accessions into an associative array
declare -A missing_accessions
while read -r accession; do
    missing_accessions["$accession"]=1
done < find_non_unitigged.txt

# Variables to keep count of the number of missing and total accessions in each size category
count_lt_10000=0
count_10000_50000=0
count_50000_100000=0
count_ge_100000=0

total_lt_10000=0
total_10000_50000=0
total_50000_100000=0
total_ge_100000=0

# Step 2: Iterate over the files in subsets/*.tsv
for file in subsets/*.tsv; do
    # Step 3: Read each line of the file
    while IFS=$'\t' read -r accession size; do
        # Categorize the accession based on its size and update total counts
        if (( size < 10000 )); then
            ((total_lt_10000++))
        elif (( size < 50000 )); then
            ((total_10000_50000++))
        elif (( size < 100000 )); then
            ((total_50000_100000++))
        else
            ((total_ge_100000++))
        fi

        # Check if the accession is in the list of missing accessions and update missing counts
        if [[ ${missing_accessions["$accession"]} ]]; then
            if (( size < 10000 )); then
                ((count_lt_10000++))
            elif (( size < 50000 )); then
                ((count_10000_50000++))
            elif (( size < 100000 )); then
                ((count_50000_100000++))
            else
                ((count_ge_100000++))
            fi
        fi
    done < "$file"
done

# Step 4: Calculate and print the percentages and total counts
echo "Percentage of missing $what accessions by size:"
echo "Size < 10000: $(( 100 * count_lt_10000 / total_lt_10000 ))% ($count_lt_10000/$total_lt_10000)"
echo "Size >= 10000 and < 50000: $(( 100 * count_10000_50000 / total_10000_50000 ))% ($count_10000_50000/$total_10000_50000)"
echo "Size >= 50000 and < 100000: $(( 100 * count_50000_100000 / total_50000_100000 ))% ($count_50000_100000/$total_50000_100000)"
echo "Size >= 100000: $(( 100 * count_ge_100000 / total_ge_100000 ))% ($count_ge_100000/$total_ge_100000)"

echo "To investigate the sizes, type: grep -Fwf find_non_unitigged.txt subsets/*.tsv |sort -n -k2 | column -t"
