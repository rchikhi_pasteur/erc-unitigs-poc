import sys

def process_accessions(file_path, group_size=20):
    with open(file_path, 'r') as file:
        small_accessions = []  # Store accessions with size < 100
        
        for line in file:
            columns = line.strip().split('\t')
            if len(columns) >= 2 and columns[0] != "Run" and columns[0] != "acc":
                accession, size = columns[0], int(columns[1])
                if size < 100:
                    small_accessions.append(accession)
                else:
                    print(f"{accession}\t{size}")  # Single accessions with size >= 100 are output immediately
        
        # Output maximized groups of small accessions
        for i in range(0, len(small_accessions), group_size):
            print(f"{','.join(small_accessions[i:i+group_size])}\t1")

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python helper.py <path_to_tsv_file>")
    else:
        process_accessions(sys.argv[1])
