import boto3, sys


def fetch_all_log_events(logs_client, log_group_name, log_stream_name):
    next_token = None

    while True:
        if next_token:
            response = logs_client.get_log_events(
                logGroupName=log_group_name,
                logStreamName=log_stream_name,
                nextToken=next_token
            )
        else:
            response = logs_client.get_log_events(
                logGroupName=log_group_name,
                logStreamName=log_stream_name,
                startFromHead=True
            )

        for event in response['events']:
            print(event['message'])

        if 'nextForwardToken' in response and response['nextForwardToken'] != next_token:
            next_token = response['nextForwardToken']
        else:
            break

def get_batch_logs(job_id):
    # Initialize clients for Batch and CloudWatch Logs
    batch_client = boto3.client('batch')
    logs_client = boto3.client('logs')

    # Get the job details
    response = batch_client.describe_jobs(jobs=[job_id])
    job_detail = response['jobs'][0]

    # Extract the log group name and log stream name
    log_stream_name = job_detail['container']['logStreamName']

    # Try to get log group from the job detail, if not available use default
    log_group_name = job_detail['container'].get('logConfiguration', {}).get('logGroupName', '/aws/batch/job')

    fetch_all_log_events(logs_client, log_group_name, log_stream_name)

if __name__ == "__main__":
    job_id = sys.argv[1]
    get_batch_logs(job_id)
